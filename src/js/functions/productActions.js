import {currencyConversion} from './convert-currency'

// ------- получение данных по товарам
export const fetchProducts = function (url) {
    return fetch(url).then((response) => response.json()).then((data) => data)
}

// ------- рендеринг товаров в HTML разметке
export const renderProducts = function (data, blockListElement, blockItemClass, initialProduct, finalProduct) {
    let newImagesArray = [],
        currency = localStorage.getItem('currency') || '₽',
        old_currency = currency,
        blockProduct = ''
    if (finalProduct > data.length) finalProduct = data.length
    // цикл только по заданному отрезку выборки данных
    for (let i = initialProduct; i < finalProduct; i++) {
        let id = data[i]['id'],
            main_image = data[i]['main_image'] || '/img/products/!no_photo.png',
            title = data[i]['title'],
            price = data[i]['price'] || 'ожидается',
            old_price = data[i]['old_price']
        if (price === 'ожидается') {
            currency = ''
            blockProduct = 'js-no-price'
        } else {
            currency = '₽'
            blockProduct = ''
        }
        if (!old_price) {
            old_price = ''
            old_currency = ''
        } else {
            old_currency = '₽'
        }
        blockListElement.insertAdjacentHTML('beforeend', `
            <li class="${blockItemClass}">
                <article class="product ${blockProduct}" data-id="${id}">
                    <div class="product__image">
                        <img src="${main_image}" alt="${title}">
                    </div>
                    <h3 class="product__title">
                        <a href="/product/${id}">${title}</a>
                    </h3>
                    <div class="product-custom">
                        <div class="product-custom__price">
                            <span class="js-conventional-unit">${currency}</span><span class="product__price">${price}</span>
                            <span class="js-conventional-unit product__price-cu">${old_currency}</span><span class="product__old-price">${old_price}</span>
                        </div>
                        <div class="product-custom__cart">
                            <button class="product-custom__cart-btn js-to-cart">
                                <svg class="product-custom__cart-icon">
                                    <use xlink:href="/img/sprite.svg#cart"></use>
                                </svg>
                                <svg class="product-custom__cart-icon--full">
                                    <use xlink:href="/img/sprite.svg#cart-full"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </article>
            </li>                        
        `)
        newImagesArray.push(blockListElement.querySelector(`[data-id="${id}"] img`))
    }
    // currencyConversion()
    return newImagesArray
}

// -------  анализ содержимого корзины и отключение кнопки add to cart на этих элементах
export const disableProducts = () => {
    let localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach(a => {
        if (document.querySelector(`.product[data-id="${a.id}"]`)) {
            document.querySelector(`.product[data-id="${a.id}"]`).classList.add('product--added')
        }
    })
}

// -------  получение массива продуктов из localstorage
export const getProductsFromLocalStorage = () => {
    let localStore = JSON.parse(localStorage.getItem('cart')) || []
    let products = []
    localStore.forEach(el => {
        products.push({
            'id': el['id'],
            'title': el['title'],
            'quantity': el['quantity']
        })
    })
    return products
}