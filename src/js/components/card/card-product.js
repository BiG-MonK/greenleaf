import vars from '../../_vars'
import {stepper} from '../../functions/stepper'

if (vars.$stepper) {
    const stepperInput = vars.$stepper.querySelector('.stepper__input'),
          stepperMinus = vars.$stepper.querySelector('.stepper__btn--minus'),
          stepperPlus = vars.$stepper.querySelector('.stepper__btn--plus')

    stepper(stepperInput, stepperMinus, stepperPlus)
}

if (document.querySelector('.card')) {
    const
        mainImageOne = vars.$cardSlider.querySelector('.card-slider__main-img--one'),
        mainImageTwo = vars.$cardSlider.querySelector('.card-slider__main-img--two'),
        cardSliderThumbs = vars.$cardSlider.querySelector('.card-slider__thumbs'),
        btnToCheckout = vars.$cardInfo.querySelector('.card-info__btn--tocheckout')
    let currentMainImage = mainImageTwo

    // ------- обработчик переключения второстепенной картинки на основную по клику
    cardSliderThumbs.addEventListener('click', e => {
        if (e.target.classList.contains('card-slider__thumb')) {
            cardSliderThumbs.querySelectorAll('.card-slider__thumb').forEach(el => {
                el.classList.remove('card-slider__thumb--active')
            })
            let src = e.target.querySelector('img').getAttribute('src')
            currentMainImage.setAttribute('src', src)
            currentMainImage = currentMainImage === mainImageOne ? mainImageTwo : mainImageOne
            document.querySelectorAll('.card-slider__main-img').forEach(el => {
                el.classList.toggle('card-slider__main-img--hidden')
            })
            e.target.classList.add('card-slider__thumb--active')
        }
    })
}