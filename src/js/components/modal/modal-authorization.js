import vars from '../../_vars';
import {modalWindowOn, closeModalWindows} from '../../functions/modalWindows'
import {clearingFormFields, isValidate} from '../../functions/validation'
import {showCallbackMessage} from "../callback-message";

if (vars.$authorization) {
    const
        loginForm = vars.$authorizationModal.querySelector('.js-modal-login'),
        regForm = vars.$authorizationModal.querySelector('.js-modal-registration'),
        recoverForm = vars.$authorizationModal.querySelector('.js-modal-recover'),
        loginPassField = vars.$authorizationModal.querySelector('.login-block__password'),
        callBackMessageAuth = vars.$authorizationModal.querySelector('.callback-message'),
        blockFormsAuth = vars.$authorizationModal.querySelector('.authorization-modal__content-wrapper'),
        lostPassRef = vars.$authorizationModal.querySelector('.login-block__lost'),
        authTabLogin = vars.$authorizationModal.querySelector('.js-tab-login'),
        authTabReg = vars.$authorizationModal.querySelector('.js-tab-reg'),
        authContentLogin = vars.$authorizationModal.querySelector('.login-block'),
        authContentReg = vars.$authorizationModal.querySelector('.reg-block'),
        authContentRecover = vars.$authorizationModal.querySelector('.recover-block'),
        authContentBlocks = vars.$authorizationModal.querySelectorAll('.authorization-modal__tab-block'),
        AUTH_FORM_HIDDEN_CLASS = 'authorization-modal__content-wrapper--hidden',
        ERROR_EMAIL_TEXT = 'Не верно указан email адрес',
        ERROR_PASS_TEXT = 'Пароль должен содержать хотя бы одно число и состоять минимум из 8 символов'

    // ------- таймер отсчета секунд до перехода на сервис почты
    const timerRedirect = () => {
        let timerCountEl = document.querySelector('.js-reg-redirect')
        let count = 10
        let timerSec =
            setInterval(() => {
                timerCountEl.textContent = count.toString()
                count--
                if (count < 0) clearInterval(timerSec)
            }, 1000)
    }

    // ------- открытие модального окна авторизации
    vars.$authorization.addEventListener('click', e => {
        modalWindowOn(vars.$authorizationModal, 'authorization-modal--visible')
        let activeTab = vars.$authorizationModal.querySelector('.authorization-modal__tab--active')
        if (activeTab) activeTab.focus()
    })

    // ------- Fix закрытия модального окна (при нажатии не на оверлее и отпускании на оверлее было закрытие)
    let mouseupElement, mousedownElement
    vars.$authorizationModal.addEventListener('mouseup', (e) => {
        mouseupElement = e.target
    })
    vars.$authorizationModal.addEventListener('mousedown', (e) => {
        mousedownElement = e.target
    })

    // ------- обработчик клика на всем окне авторизации
    vars.$authorizationModal.addEventListener('click', e => {
        // ------- закрытие модального окна авторизации
        let closeWindowByOverlay = false
        if ((window.innerWidth < 1024) && e.target.classList.contains('authorization-modal')) closeWindowByOverlay = true
        if ((e.target.closest('.authorization-modal__close') || closeWindowByOverlay)
        && mouseupElement === mousedownElement) {   // при нажатии и отпускании кнопке на одном и том же элементе
            closeModalWindows()
            clearingFormFields(loginForm)
            clearingFormFields(regForm)
            clearingFormFields(recoverForm)
        }
        // --- клик по вкладке "ВХОД" или кнопке "УЖЕ ЕСТЬ АККАУНТ"
        if (e.target === authTabLogin || e.target.classList.contains('reg-block__have-acc')) {
            authTabLogin.classList.add('authorization-modal__tab--active')
            authTabReg.classList.remove('authorization-modal__tab--active')
            authContentBlocks.forEach(tab => tab.classList.add('authorization-modal__tab-block--fade'))
            authContentLogin.classList.remove('authorization-modal__tab-block--fade')
        }
        // --- клик по вкладке "РЕГИСТРАЦИЯ" или кнопке "У НАС ВПЕРВЫЕ"
        if (e.target === authTabReg || e.target.classList.contains('login-block__create-acc')) {
            authTabLogin.classList.remove('authorization-modal__tab--active')
            authTabReg.classList.add('authorization-modal__tab--active')
            authContentBlocks.forEach(tab => tab.classList.add('authorization-modal__tab-block--fade'))
            authContentReg.classList.remove('authorization-modal__tab-block--fade')
        }
        // --- клик по ссылке "Забыли пароль"
        if (e.target === lostPassRef) {
            authTabLogin.classList.remove('authorization-modal__tab--active')
            authTabReg.classList.remove('authorization-modal__tab--active')
            authContentBlocks.forEach(tab => tab.classList.add('authorization-modal__tab-block--fade'))
            authContentRecover.classList.remove('authorization-modal__tab-block--fade')
        }
    })

    // ------- submit формы ЛОГИНА модального окна авторизации
    loginForm.addEventListener('submit', e => {
        e.preventDefault()
        vars.$loader.classList.remove('is-delete')       // делаем видимым лоадер
        let formData = new FormData(loginForm)
        fetch("/login", {
            method: "POST",
            body: formData
        })
            .then((response) => response.json())
            .then((result) => {
                if (result['is_ok']) {
                    callBackMessageAuth.classList.remove('callback-message--error')
                    setTimeout(() => {
                        window.location.href = '/profile'
                    }, 4000)
                } else callBackMessageAuth.classList.add('callback-message--error')
                setTimeout(() => {
                    showCallbackMessage(blockFormsAuth, AUTH_FORM_HIDDEN_CLASS, callBackMessageAuth, result['message'])
                        .catch(err => console.error(err))
                    vars.$loader.classList.add('is-delete')
                    loginPassField.value = ''
                    loginPassField.focus()
                }, 500)
            })
            .catch(err => console.error(err))
    })

    // ------- submit формы РЕГИСТРАЦИИ модального окна авторизации
    regForm.addEventListener('submit', e => {
        const email = vars.$authorizationModal.querySelector('.reg-block__email')
        const password = vars.$authorizationModal.querySelector('.reg-block__password')
        e.preventDefault()
        vars.$loader.classList.remove('is-delete')       // делаем видимым лоадер
        let formData = new FormData(regForm)
        if (!isValidate(email)) {
            callBackMessageAuth.classList.add('callback-message--error')
            vars.$loader.classList.add('is-delete')
            showCallbackMessage(blockFormsAuth, AUTH_FORM_HIDDEN_CLASS, callBackMessageAuth, ERROR_EMAIL_TEXT)
                .catch(err => console.error(err))
        } else {
            if (isValidate(password)) {
                fetch("/registration", {
                    method: "POST",
                    body: formData
                })
                    .then((response) => response.json())
                    .then((result) => {
                        let text = result['message'], actionFunction, showTime
                        if (result['is_ok']) {
                            callBackMessageAuth.classList.remove('callback-message--error')
                            text = `
                                <h3>Аккаунт успешно создан!</h3>
                                <p class="callback-message__paragraph">На ваш Email отправлено проверочное письмо.</p>
                                <p class="callback-message__paragraph">Для окончания регистрации, перейдите по ссылке в нем.</p>
                                <p class="callback-message__paragraph">Переход на почту через <br /><span class="callback-message__timer js-reg-redirect"></span> сек.</p>
                            `
                            actionFunction = timerRedirect
                            showTime = 12000
                        } else callBackMessageAuth.classList.add('callback-message--error')
                        setTimeout(() => {
                            vars.$loader.classList.add('is-delete')
                            showCallbackMessage(blockFormsAuth, AUTH_FORM_HIDDEN_CLASS, callBackMessageAuth, text, actionFunction, showTime)
                                .then(() => {
                                    if (result['is_ok']) window.location = "https://" + result['message']
                                })
                                .catch(err => console.error(err))
                        }, 500)
                    })
                    .catch(err => console.error(err))
            } else {
                callBackMessageAuth.classList.add('callback-message--error')
                vars.$loader.classList.add('is-delete')
                showCallbackMessage(blockFormsAuth, AUTH_FORM_HIDDEN_CLASS, callBackMessageAuth, ERROR_PASS_TEXT)
                    .catch(err => console.error(err))
            }
        }
    })

    // ------- submit формы ВОССТАНОВЛЕНИЯ ПАРОЛЯ модального окна авторизации
    recoverForm.addEventListener('submit', e => {
        const email = vars.$authorizationModal.querySelector('.recover-block__email')
        e.preventDefault()
        vars.$loader.classList.remove('is-delete')       // делаем видимым лоадер
        let formData = new FormData(recoverForm)
        if (!isValidate(email)) {
            callBackMessageAuth.classList.add('callback-message--error')
            vars.$loader.classList.add('is-delete')
            showCallbackMessage(blockFormsAuth, AUTH_FORM_HIDDEN_CLASS, callBackMessageAuth, ERROR_EMAIL_TEXT)
                .catch(err => console.error(err))
        } else {
            fetch("/recover", {
                method: "POST",
                body: formData
            })
                .then((response) => response.json())
                .then((result) => {
                    let text = result['message'], actionFunction, showTime
                    if (result['is_ok']) {
                        callBackMessageAuth.classList.remove('callback-message--error')
                        text = `
                            <h3>Запрос успешно обработан!</h3>
                            <p class="callback-message__paragraph">На ваш Email отправлено письмо.</p>
                            <p class="callback-message__paragraph">Для завершения процедуры восстановления пароля, перейдите по ссылке в нем.</p>
                            <p class="callback-message__paragraph">Переход на почту через <br /><span class="callback-message__timer js-reg-redirect"></span> сек.</p>
                        `
                        actionFunction = timerRedirect
                        showTime = 12000
                    } else callBackMessageAuth.classList.add('callback-message--error')
                    setTimeout(() => {
                        vars.$loader.classList.add('is-delete')
                        showCallbackMessage(blockFormsAuth, AUTH_FORM_HIDDEN_CLASS, callBackMessageAuth, text, actionFunction, showTime)
                            .then(() => {
                                if (result['is_ok']) window.location = "https://" + result['message']
                            })
                            .catch(err => console.error(err))
                    }, 1500)
                })
                .catch(err => console.error(err))
        }
    })
}