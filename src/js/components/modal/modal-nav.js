import vars from '../../_vars';
import {modalWindowOn, closeModalWindows} from '../../functions/modalWindows'

if (vars.$navModal) {

    // открытие модального бургер меню
    vars.$burger.addEventListener('click', () => {
        modalWindowOn(vars.$navModal, 'nav-modal--visible')
    })

    // закрытие модального бургер меню
    vars.$navModal.addEventListener('click', e => {
        if (e.target.classList.contains('nav-modal__link')
            || e.target.classList.contains('nav-modal__close')
            || e.target.classList.contains('nav-modal__close-icon')) {
            closeModalWindows()
        }
    })
}