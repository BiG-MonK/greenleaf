import vars from '../../_vars';
import {closeModalWindows, modalWindowOn} from "../../functions/modalWindows";

const
    policyRefs = document.querySelectorAll('.js-policy-ref'),
    policyClose = document.querySelector('.policy-modal .modal__close')

policyRefs.forEach(el => {
    el.addEventListener('click', e => {
        e.preventDefault()
        modalWindowOn(vars.$policyModal, 'policy-modal--visible')
    })
})

policyClose.addEventListener('click', e => {
    closeModalWindows()
})