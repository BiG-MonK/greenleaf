const
    DELAY_SHOW_MESSAGE = 1000,   // после сокрытия формы показать сообщение через данный период
    DELAY_HIDDEN_MESSAGE = 4000,   // после показа сообщения скрыть его через данный период
    DELAY_SHOW_FORM = 1000   // после сокрытия собщения показать форму через данный период


// ------- появление скрытого окна оверлея с сообщением на формах
export const showCallbackMessage = (block, formHiddenClass, callbackMessage, textMessage, action = () => {}, delayBeforeHidden = DELAY_HIDDEN_MESSAGE) => {
    const callbackMessageText = callbackMessage.querySelector('.js-callback-message__text')
    callbackMessageText.innerHTML = textMessage
    block.classList.add(formHiddenClass)
    return new Promise((resolve) => {
        setTimeout(() => {                      // через 1000мс после сокрытия формы показать сообщение
            callbackMessage.classList.add('callback-message--visible')
            action()
            setTimeout(() => {                  // через 4000мс после показа сообщения скрыть его
                callbackMessage.classList.remove('callback-message--visible')
                setTimeout(() => {              // через 1000мс после сокрытия собщения показать форму
                    block.classList.remove(formHiddenClass)
                    resolve()
                }, DELAY_SHOW_FORM)
            }, delayBeforeHidden)
        }, DELAY_SHOW_MESSAGE)
    })
}