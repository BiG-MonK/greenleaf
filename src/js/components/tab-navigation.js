import vars from "../_vars";

// ------- нажатие навигационного меню на странице главной корзины
vars.$tabNavigationLinks.forEach(el => {
    el.addEventListener('click', e => {
        const activeLink = vars.$tabNavigationList.querySelector('.tab-navigation__link--active'),
            newActiveLink = e.target,
            activeContent = vars.$tabContent.querySelector('.tab-content__tab--active'),
            newActiveContent = vars.$tabContent.querySelector(`[data-section="${newActiveLink.getAttribute('href')}"]`),
            tabContentPaddingTop =  parseInt(getComputedStyle(vars.$tabContent).paddingTop),
            tabContentPaddingBottom =  parseInt(getComputedStyle(vars.$tabContent).paddingBottom)
        let activeContentHeight = parseInt(getComputedStyle(activeContent).height)
        vars.$tabContent.style.height = (activeContentHeight + tabContentPaddingTop + tabContentPaddingBottom) + 'px'
        e.preventDefault()
        
        // для истории браузера (переход только после обновления страницы)
        window.history.pushState("object or string", "Title", newActiveLink.dataset.url)
        // window.location.assign(newActiveLink.dataset.url)

        if (activeLink !== newActiveLink) {
            activeLink.classList.remove('tab-navigation__link--active')
            newActiveLink.classList.add('tab-navigation__link--active')
            activeContent.classList.remove('tab-content__tab--active')
            vars.$tabNavigationLinks.forEach(el => {
                el.classList.add('is-disabled')
            })
            setTimeout(() => {
                activeContent.classList.add('is-delete')
                newActiveContent.classList.remove('is-delete')
                activeContentHeight = parseInt(getComputedStyle(newActiveContent).height)
                vars.$tabContent.style.height = (activeContentHeight + tabContentPaddingTop + tabContentPaddingBottom) + 'px'
                setTimeout(() => {
                    newActiveContent.classList.add('tab-content__tab--active')
                    vars.$tabContent.style.height = 'auto'
                    vars.$tabNavigationLinks.forEach(el => {
                        el.classList.remove('is-disabled')
                    })
                }, 750)
                setTimeout(() => {
                    newActiveContent.classList.add('tab-content__tab--active')
                }, 10)
            }, 500)
        }
    })
})