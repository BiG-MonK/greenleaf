<?php

$responseMessage = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fullName = cleaner($_POST['full_name']);
    $timeIsNow = date('d/m/Y H:i:s', time());

    if (URL_PARAMS['action'] === 'phone') {
        $phone = cleaner($_POST['phone']);
        if (validateFullName($fullName) && validatePhone($phone)) {
            // отправка почты администратору
            include('php/controllers/mail/callback-phone-mail.php');
        }
    }
    if (URL_PARAMS['action'] === 'question') {
        $text = cleaner($_POST['text']);
        $email = cleaner($_POST['email']);
        if (validateTextArea($text) && validateFullName($fullName) && validateEmail($email)) {
            // отправка почты администратору
            include('php/controllers/mail/callback-question-mail.php');
        }
    }
}

echo json_encode($responseMessage);
