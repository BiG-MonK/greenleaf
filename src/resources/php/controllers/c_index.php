<?php

    // Ответ getMainCategoriesInfo()
    // -----
        // "id": "1",
        // "name": "Чистящие средства для дома",
        // "translit": "Chistyashchie_sredstva_dlya_doma",
        // "description": null,
        // "desc_short": "Новейшие эффективные формулы...",
        // "main_img": "/img/main/main-category_1.jpg",
        // "mini_img": "/img/catalog/filter-category_1.jpg"
    // -----
    $mainUrl = true;          // для смены шапки в header.php и активации навигационной ссылки в блоке header
    $categories = getMainCategoriesInfo();

    include('php/views/index.php');