<?php
// --- Письмо для админа сайта об изменении статуса заказа

$mail -> ClearAddresses();

// -- Кому отправить
$mail -> addAddress(EMAIL_ADMIN);

// -- Тема письма
$mail -> Subject = "Заказу №_" . $id_order . " присвоен статус " . $orderInfo['status_name'];

// --- Тело письма
$body = "
            <h2>Изменение статуса!</h2>
            <p>
                <strong>Заказ №_" . $id_order . "</strong>, изменил свой статус на 
                <strong style='text-transform: uppercase'>" . $orderInfo['status_name'] . "</strong>.
            </p>
            <div><strong>Заказ сделан:</strong> ".$orderInfo['date_placed']."</div>
            <div><strong>Клиент:</strong> ".$userName."</div>
            <div><strong>Телефон:</strong> ".$userInfoForEmail['phone']."</div>
            <div><strong>eMail:</strong> ".$userInfoForEmail['email']."</div>
            <div><strong>Метод доставки:</strong> ".$methodDeliveryRus."</div>
            ".$deliveryHTMLString."
            <div><strong>Заказанный товар:</strong></div>
            <ul>
                ".$productsInfoStringAdmin."
            </ul>
            <div><strong>Общая сумма(с учетом доставки): </strong>  ".$orderInfo['sum_total_rub']." руб.</div>";

$mail->Body = $body;

// Отправляем
if (!$mail -> send()) {
    $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка отправки почты!'];
} else {
    $responseMessage = ['is_ok' => 1, 'message' => 'Почта отправлена успешно!'];
}