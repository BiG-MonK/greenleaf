<?php
// --- Письмо для клиента сайта об успешном заказе

// -- Кому отправить
$mail -> addAddress($email);

// -- Тема письма
$mail -> Subject = "Заказ №_".$lastIdOrder." получен";

// --- Тело письма
$body = "   <h2>Здравствуйте!</h2>
            <p><strong>Заказ №_".$lastIdOrder."</strong> от ".$date_placed_full." на сумму <strong>".$sum_total_rub.".00 руб.</strong> принят.</p>
            
            <div>Состав заказа:
                <div>Клиент: ".$userName."</div>
                <div>Метод доставки: ".$methodDeliveryRus."</div>
                <div>Заказанный товар:</div>
                <ul>
                    ".$productsInfoStringClient."
                </ul>
                <div>Доставка: ".$sumDelivery.".00 руб.</div>
                ".$deliveryHTMLString."
            </div>
            <p><strong>Спасибо за заказ!</strong> В ближайшее время мы свяжемся с Вами.</p>
            
            <div style='margin-top: 40px'>Это письмо сгенерировано автоматически, 
                если хотите с нами связаться, то вот наши контакты:</div>
            <div><em>Телефон:</em> ".PHONE_ADMIN."</div>
            <div><em>Почта:</em> ".EMAIL_ADMIN."</div>";

$mail -> Body = $body;

// Отправляем
if ($mail -> send()) {
    $responseMessage = ['is_ok' => 1, 'message' => 'Заказ успешно создан!'];
} else {
    $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка создания заказа!'];
}