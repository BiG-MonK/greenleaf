<?php
// --- Письмо для администратора сайта о поступлении вопроса

$mail -> ClearAddresses();

// -- Кому отправить
$mail -> addAddress(EMAIL_ADMIN);

// -- Тема письма
$mail -> Subject = "Клиент задает вопрос!";

// --- Тело письма
$body = "   <h2>Поступил вопрос</h2>
            <div><strong>Содержание вопроса:</strong><p>".$text."</p></div>
            <div><strong>Дата поступления вопроса:</strong> ".$timeIsNow."</div>
            <div><strong>Клиент:</strong> ".$fullName."</div>
            <div><strong>eMail:</strong> ".$email."</div>";

$mail -> Body = $body;

if (!$mail -> send()) {
    $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка отправки почты!'];
} else {
    $responseMessage = ['is_ok' => 1, 'message' => 'Почта отправлена успешно!'];
}