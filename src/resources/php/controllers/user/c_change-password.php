<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $currentPass = $_POST['current_password'];
        $newPass = $_POST['new_password'];
        $confirmPass = $_POST['confirm_password'];
        $hash = cleaner($_POST['hash']);

        if (isset($_POST['hash'])) {                    // если в запросе присут-ет hash(значит со страницы восстановления)
            $userId = getUserIdByRecoverHash($hash);    // ищем hash в БД и выдаем id юзера
            if ($userId) {                              // если такой юзер найден по этому hash в БД
                $newPassHash = password_hash($newPass, PASSWORD_BCRYPT);    // хэшируем новый пароль
                if (updateUserPassword($userId, $newPassHash)) {        // пишем его в БД вместо старого
                    $responseMessage = ['is_ok' => 1, 'message' => 'Пароль успешно изменен!'];
                    deleteRecoverHashByUserId($userId);                 // удаляем hash у этого пользователя
                    $token = substr(bin2hex(random_bytes(128)), 0, 128);    // генерируем токен для авторизации
                    addNewSession($userId, $token);                                         // создаем новую сессию
                    $_SESSION['token'] = $token;
                    setcookie('token', $token, time() + 3600 * 24 * 30 * 6, '/');           // создаем куку на полгода
                } else $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка запроса к базе данных'];
            } else {        // если вдруг hash подменен и его нет в БД
                $isRecovery = false;
                include('php/views/recover.php');
                exit();
            }
        } else {
            if (isset($user)) {     // если юзер авторизован
                if (!empty($currentPass) && !empty($newPass) && !empty($confirmPass)) {
                    if (validatePassword($newPass)) {

                        if (password_verify($currentPass, $user['password'])) {
                            $newPassHash = password_hash($newPass, PASSWORD_BCRYPT);
                            if (updateUserPassword($user['id_user'], $newPassHash)) {
                                $responseMessage = ['is_ok' => 1, 'message' => 'Пароль успешно изменен!'];
                            } else $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка запроса к базе данных'];
                        } else $responseMessage = ['is_ok' => 0, 'message' => 'Введенный действующий пароль не верен!'];

                    } else $responseMessage = ['is_ok' => 0, 'message' => 'Пароль должен содержать минимум 8 символов и одно число'];
                } else $responseMessage = ['is_ok' => 0, 'message' => 'Заполните все поля формы'];
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Для смены пароля необходимо авторизоваться'];
        }

        echo json_encode($responseMessage);
    }
    exit();