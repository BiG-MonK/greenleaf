<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $country = cleaner($_POST['country']);
        $city = cleaner($_POST['city']);
        $street = cleaner($_POST['street']);
        $postcode = cleaner($_POST['postcode']);

        if (!empty($country) && !empty($city) && !empty($street) && !empty($postcode)) {
            if (validateCountry($country)) {
                if (validateCity($city)) {
                    if (validateStreet($street)) {
                        if (validatePostcode($postcode)) {

                            if (updateUserAddress($user['id_user'], $country, $city, $street, $postcode)) {
                                $responseMessage = ['is_ok' => 1, 'message' => 'Данные доставки успешно сохранены!'];
                            } else $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка изменения данных доставки!'];
                            $responseMessage['dataInfo'] = $_POST;

                        } else $responseMessage = ['is_ok' => 0, 'message' => 'Почтовый индекс указан некорректно'];
                    } else $responseMessage = ['is_ok' => 0, 'message' => 'Адрес указан некорректно'];
                } else $responseMessage = ['is_ok' => 0, 'message' => 'Населенный пункт указан некорректно'];
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Страна указана некорректно'];
        } else $responseMessage = ['is_ok' => 0, 'message' => 'Заполните все поля формы'];

        echo json_encode($responseMessage);
    }
    exit();