<?php

// используется URL_PARAMS['id'] в файле catalog-grid.php из main.php
// используется URL_PARAMS['id'] в файле hero-catalog.php из main.php

// используется URL_PARAMS['subcategories'] в файле catalog-grid.php

    // Ответ getMainCategoriesInfo()
    // -----
    // "id": "1",
    // "name": "Чистящие средства для дома",
    // "translit": "Chistyashchie_sredstva_dlya_doma",
    // "description": null,
    // "desc_short": "Новейшие эффективные формулы...",
    // "main_img": "/img/main/main-category_1.jpg",
    // "mini_img": "/img/catalog/filter-category_1.jpg"
    // -----

    $categoryColors = [
        '1' => 'clean',
        '2' => 'blue',
        '3' => 'pink',
        '4' => 'red',
        '5' => 'turquoise',
        '6' => 'green',
        '7' => 'purple',
        '8' => 'yellow',
        '9' => 'beige',
    ];
    $categories = getMainCategoriesInfo();
    $currentIdCategory = intval(cleaner(URL_PARAMS['id']));
    $currentNameSubCategory = cleaner(URL_PARAMS['subcategories']);
    $categoriesInfo = [];

    // если категория с таким ID существует..
    if (!empty(URL_PARAMS['id']) && ($currentIdCategory > 0) && ($currentIdCategory <= count($categories))) {
        // если параметра с названием подкатегории нет или он такая подкатегория есть в БД
        if (empty($currentNameSubCategory) || isSubCategory($currentIdCategory, $currentNameSubCategory)) {
            // добавление персонального цвета к каждой категории
            foreach($categories as $category) {
                $category['color'] = $categoryColors[$category['id']];
                array_push($categoriesInfo, $category);
            }
            $catalogUrl = true;                 //  для активации навигационной ссылки в блоке header
            include('php/views/catalog.php');
        } else include('php/controllers/c_error_404.php');
    } else include('php/controllers/c_error_404.php');