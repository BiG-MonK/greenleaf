<?php
/* Параметры запроса
    from	    Почтовый индекс отправителя (6 цифр)
    to	        Почтовый индекс получателя (6 цифр)
    mass	    Масса отправления, в граммах (значения от 0 до 20000)
    valuation	Объявленная ценность, в рублях
    vat	        НДС 20% (1 — с НДС, 0 — без НДС)
    oversized	Негабаритная посылка (1 — негабаритная, 0 — обычная)
*/

/*  Расшифровка ответа сервера сервиса https://postprice.ru/api/#russia
    code            Внутренний код ответа
    pkg             Стоимость посылки, в рублях
    pkg_1class	    Стоимость заказной посылки 1 класса, в рублях
    pkg_val_1class	Стоимость ценной посылки 1 класса, в рублях
    city_from       Населенный пункт отправителя
    city_to	        Населенный пункт получателя
    locality_from	Наименование ОПС отправителя
    locality_to	    Наименование ОПС получателя
    region_from     Регион отправителя
    region_to       Регион получателя
    cod	            Сумма наложенного платежа, в рублях
*/

/*  Коды ошибок "code" API сервиса оценки услуги отправки почты России
    100     Расчет завершен успешно
    101	    Некорректное значение массы отправления
    102	    Некорректный индекс отправителя
    103	    Некорректный индекс получателя
    104	    Отправка в данное ОПС невозможна
    105	    Исчерпан лимит запросов
*/

$responseMessage = ['is_ok' => 0, 'response' => 'Неверный метод запроса!'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $postcodeClient = cleaner($_POST['postcode']);
    $cartItems = json_decode($_POST['cart_items']);

    if (validatePostcode($postcodeClient)) {
        // переборка входящего списка товаров для расчета их общей массы
        if (!empty($cartItems) && is_array($cartItems)) {
            $totalWeight = 0;
            foreach ($cartItems as $product) {
                $productsWeight = getWeightProduct(strval($product -> id)) * $product -> quantity;
                $totalWeight += $productsWeight;
            }
        } else {
            $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка в данных товара!'];
            echo json_encode($responseMessage);
            exit();
        }

        $url = "https://postprice.ru/engine/russia/api.php?from=" . POSTCODE_ADMIN . "&to=" . $postcodeClient . "&mass=" . $totalWeight;
        $result = file_get_contents($url);
        $result = json_decode($result);

        if ($result -> code === 100) {                                      // если все успешно
            $price = ceil(($result -> pkg + POST_MARKUP) / 10) * 10; // округление до десятков
            $responseMessage = [
                'is_ok' => 1,
                'response' => [
                    'price' => $price,
                    'weight' => round($totalWeight / 1000, 2)
                ]
            ];
        } elseif ($result -> code === 101) {
            $message = 'Некорректное значение массы отправления';
            $responseMessage = ['is_ok' => 0, 'response' => $message];
        } elseif (($result -> code === 102)) {
            $message = 'Некорректный индекс отправителя';
            $responseMessage = ['is_ok' => 0, 'response' => $message];
        } elseif (($result -> code === 103)) {
            $message = 'Некорректный индекс получателя';
            $responseMessage = ['is_ok' => 0, 'response' => $message];
        } elseif (($result -> code === 104)) {
            $message = 'Отправка в данное ОПС невозможна';
            $responseMessage = ['is_ok' => 0, 'response' => $message];
        } elseif (($result -> code === 105)) {
            $message = 'Исчерпан лимит запросов';
            $responseMessage = ['is_ok' => 0, 'response' => $message];
        }
    } else $responseMessage = ['is_ok' => 0, 'response' => 'Неверный ИНДЕКС!'];

}

echo json_encode($responseMessage);