<?php

    if (is_string(URL_PARAMS['id_product'])) {
        $all_similar = getActiveSimilarProducts(URL_PARAMS['id_product']);
        $response = [];
        foreach ($all_similar as $similar) {
            array_push($response, getProductOne($similar['id_product']));
        }
        echo json_encode($response);

    } else echo "Некорректные входные параметры!";