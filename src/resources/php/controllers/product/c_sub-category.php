<?php

    $id_category = URL_PARAMS['id_category'];
    $subCategories = explode('-', URL_PARAMS['subcategories']);
    $sort = URL_PARAMS['sort'] ? URL_PARAMS['sort'] : '';

    if (is_string($id_category) && is_array($subCategories) && is_string($sort)) {

        $resultArray = ['products' => getProductsBySubCategoryAndSort($id_category, $subCategories, $sort)];
        echo json_encode($resultArray);

    } else echo "Некорректные входные параметры!";