<?php

if (isset($_POST)) {
    $captcha = $_POST["token"];
    $secretKey = '6LfmHZcdAAAAAH1cdih6UEEiKWol26yuaFs9fDjF';
    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $captcha;

    $response = file_get_contents($url);
    $responseKeys = json_decode($response, true);
    header('content-type: application/json');

    if ($responseKeys["success"] && $responseKeys["score"] >= 0.5) {
        include_once('c_callback.php');
    } else echo json_encode(['is_ok' => 0, 'message' => 'Нам кажется, что вы робот Оо']);
}