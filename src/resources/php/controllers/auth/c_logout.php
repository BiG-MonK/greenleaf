<?php

    unset($_SESSION['token']);
    setcookie('token', null, -1, '/');
    session_unset();
    session_destroy();
    header("Location: /main.php");
    exit();
