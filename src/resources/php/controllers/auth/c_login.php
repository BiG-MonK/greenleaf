<?php

    clearFailHashAndUser();        // удаление просроченных хэшей для активации почты, восст. паролей и не активных 3суток юзеров

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $login = cleaner($_POST['email']);
        $password = $_POST['password'];
        $remember = isset($_POST['remember']);

        if (!empty($login) && !empty($password)) {
            if (validateEmail($login)) {

                $user = getUserByLogin($login);
                if (isset($user)) {
                    if (isUserActivated($user['id_user'])) {
                        if (password_verify($password, $user['password'])) {

                            $token = substr(bin2hex(random_bytes(128)), 0, 128);
                            addNewSession($user['id_user'], $token);
                            $_SESSION['token'] = $token;
                            if($remember) setcookie('token', $token, time() + 3600 * 24 * 30 * 6, '/');
                            $responseMessage = ['is_ok' => 1, 'message' => 'Авторизация прошла успешно'];

                        } else $responseMessage = ['is_ok' => 0, 'message' => 'Сочетание логина и пароля не подходит'];
                    } else $responseMessage = ['is_ok' => 0, 'message' => 'Учетная запись не активирована. Вам необходимо подтвердить Email адрес'];
                } else $responseMessage = ['is_ok' => 0, 'message' => 'Такой пользователь еще не зарегистрирован'];
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Некорректно указан email адрес'];
        } else $responseMessage = ['is_ok' => 0, 'message' => 'Заполните все поля формы'];

        echo json_encode($responseMessage);
    } else echo "Некорректный метод запроса, должен быть POST!";
    exit();
