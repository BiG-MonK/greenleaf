<?php

// очищает от ненужных символов интерактивные поля
    function cleaner ($value = "") : ?string {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return $value;
    }

// проверка длинны заполнения поля
    function isNormalLength ($value = "", $min, $max) : bool {
        $result = (mb_strlen($value) < $min || mb_strlen($value) > $max);
        return !$result;
    }

    function validatePassword ($password) : bool {
        return (preg_match(PATTERN_PASSWORD, $password) && isNormalLength($password, 8, 30));
    }

    function validateEmail ($email) : bool {
        return (preg_match(PATTERN_EMAIL, $email) && isNormalLength($email, 8, 50));
    }

    function validatePhone ($phone) : bool {
        return (preg_match(PATTERN_PHONE, $phone) && isNormalLength($phone, 6, 30));
    }

    function validateFirstName ($first_name) : bool {
        return (preg_match(PATTERN_NAME, $first_name) && isNormalLength($first_name, 2, 30));
    }

    function validateLastName ($last_name) : bool {
        return (preg_match(PATTERN_NAME, $last_name) && isNormalLength($last_name, 2, 30));
    }

    function validateMiddleName ($middle_name) : bool {
        return (preg_match(PATTERN_NAME, $middle_name) && isNormalLength($middle_name, 2, 30));
    }

    function validateFullName ($full_name) : bool {
        return (preg_match(PATTERN_NAME_GAP, $full_name) && isNormalLength($full_name, 2, 50));
    }

    function validateCountry ($country) : bool {
        return (preg_match(PATTERN_COUNTRY, $country) && isNormalLength($country, 4, 50));
    }

    function validateCity ($city) : bool {
        return (preg_match(PATTERN_CITY, $city) && isNormalLength($city, 4, 50));
    }

    function validateStreet ($street) : bool {
        return (preg_match(PATTERN_STREET, $street) && isNormalLength($street, 10, 100));
    }

    function validatePostcode ($postcode) : bool {
        return (preg_match(PATTERN_POSTCODE, $postcode) && isNormalLength($postcode, 6, 6));
    }

    function validateNotes ($notes) : string {
        return mb_strimwidth($notes, 0, 995, "...");
    }

    function validateTextArea ($text) : string {
        return (preg_match(PATTERN_TEXTAREA, $text) && isNormalLength($text, 15, 1000));
    }