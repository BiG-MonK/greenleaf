<?php

    // -- Функция для авторизации, по логину выдает id пользователя и пароль для сравнения с БД
    function getUserByLogin(string $login) : ?array {
        $sql = "SELECT id_user, password 
                    FROM users 
                    WHERE login = :login";
        $query = dbQuery($sql, ['login' => $login]);
        $user = $query -> fetch();
        return $user === false ? null : $user;
    }

    // -- Функция проверки существует ли такой email или нет
    function isEmailAlreadyExist(string $email) : bool {
        $sql = "SELECT id_user 
                    FROM users
                    WHERE email = :email LIMIT 1";
        $query = dbQuery($sql, ['email' => $email]);
        $isExist = $query -> fetch();
        return $isExist === false ? false : true;
    }

    // -- Функция проверки активирован ли пользователь или нет по его Id
    function isUserActivated(string $id) : bool {
        $sql = "SELECT users.id_user
                FROM users
                WHERE users.id_user = :id AND users.is_verified = 1";
        $query = dbQuery($sql, ['id' => $id]);
        $isActivated = $query -> fetch();
        return $isActivated === false ? false : true;
    }

    // -- Функция удаления просроченных не активированных пользователей по истечению времени
    function deleteUsersByTime() : bool {
        $sql = "DELETE FROM users WHERE users.is_verified = 0 AND date_reg < (NOW() - INTERVAL 72 HOUR )";
        dbQuery($sql);
        return true;
    }

    // -- Функция изменения статуса активации аккаунта, после подтверждения верификации письма
    function isVerifiedUserUpdate(string $email) : bool {
        $sql = "UPDATE users SET is_verified = 1 WHERE users.email = :email";
        $params = ['email' => $email];
        dbQuery($sql, $params);
        return true;
    }

// ---------------------------  Работа с verify HASH (верификация письма)
    // -- Функция добавления пользователю хэша для верификации письма
    function addNewVerifyHash(string $id, string $hash) : bool {
        $sql = "INSERT verify_hash (id_user, hash) VALUES (:id, :hash)";
        $params = ['id' => $id, 'hash' => $hash];
        dbQuery($sql, $params);
        return true;
    }

    // -- Функция проверки существует ли такой hash по этому email или нет
    function getUserByVerifyHash(string $email, string $hash) : ?string {
        $sql = "SELECT users.id_user
                FROM users
                INNER JOIN verify_hash ON verify_hash.id_user = users.id_user
                WHERE users.email = :email AND verify_hash.hash = :hash";
        $query = dbQuery($sql, ['email' => $email, 'hash' => $hash]);
        $user = $query -> fetch();
        return $user['id_user'];
    }

    // -- Функция удаления hash по истечению времени
    function deleteVerifyHashByTime() : bool {
        $sql = "DELETE FROM verify_hash WHERE verify_hash.hash_time < (NOW() - INTERVAL 72 HOUR)";
        dbQuery($sql);
        return true;
    }

    // -- Функция удаления hash после активации аккаунта
    function deleteVerifyHashAfterActivation(string $hash) : bool {
        $sql = "DELETE FROM verify_hash WHERE verify_hash.hash = :hash";
        $params = ['hash' => $hash];
        dbQuery($sql, $params);
        return true;
    }

// ---------------------------  Работа с recover HASH (восстановления пароля)
    // -- Функция добавления пользователю хэша для верификации ссылки восстановления пароля
    function addNewRecoverHash(string $id, string $hash) : bool {
        $sql = "INSERT recover_hash (id_user, hash) VALUES (:id, :hash)";
        $params = ['id' => $id, 'hash' => $hash];
        dbQuery($sql, $params);
        return true;
    }

    // -- Функция проверки существует ли такой recover hash по этому email или нет
    function getUserIdByRecoverHashAndEmail(string $email, string $hash) : ?string {
        $sql = "SELECT users.id_user
                FROM users
                INNER JOIN recover_hash ON recover_hash.id_user = users.id_user
                WHERE users.email = :email AND recover_hash.hash = :hash";
        $query = dbQuery($sql, ['email' => $email, 'hash' => $hash]);
        $user = $query -> fetch();
        return $user['id_user'];
    }

    // -- Функция проверки существует ли такой юзер по этому hash в БД или нет
    function getUserIdByRecoverHash(string $hash) : ?string {
        $sql = "SELECT id_user 
                FROM recover_hash
                WHERE recover_hash.hash = :hash";
        $query = dbQuery($sql, ['hash' => $hash]);
        $user = $query -> fetch();
        return $user['id_user'];
    }

    // -- Функция удаления recover hash по истечению времени
    function deleteRecoverHashByTime() : bool {
        $sql = "DELETE FROM recover_hash WHERE recover_hash.hash_time < (NOW() - INTERVAL 24 HOUR)";
        dbQuery($sql);
        return true;
    }

    // -- Функция удаления всех recover hash у конкретного пользователя по его id
    function deleteRecoverHashByUserId(string $id) : bool {
        $sql = "DELETE FROM recover_hash WHERE recover_hash.id_user = :id";
        $params = ['id' => $id];
        dbQuery($sql, $params);
        return true;
    }