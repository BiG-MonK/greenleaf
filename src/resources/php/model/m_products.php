<?php

// Выдает продукты относящиеся к определенной категории с учетом is_active
function getProductsByCategory(string $id_category) : ?array {
    $sql = "SELECT DISTINCT id, title, price, old_price, main_image
            FROM (
                SELECT products.id_product AS id, products.name AS title, 
                       products.main_image AS main_image, products.price, products.old_price, is_active
                FROM categories
                INNER JOIN productsandcategories ON productsandcategories.id_category = categories.id_category
                INNER JOIN products ON products.id_product = productsandcategories.id_product
                WHERE categories.parent_id = :id
            ) AS a
            LEFT JOIN product_photos ON product_photos.id_product = id
            WHERE a.is_active = 1
            ORDER BY main_image DESC, price DESC";
    $query = dbQuery($sql, ['id' => $id_category]);
    $products = $query -> fetchAll();
    return $products === false ? null : $products;
}

// Выдает продукты относящиеся к определенной группе на главной странице с учетом is_active
function getProductsByGroup(string $group) : ?array {
    $temp = 'is_popular';
    if ($group === 'new') { $temp = 'is_new'; }
    if ($group === 'sale') { $temp = 'is_sale'; }
    $sql = "SELECT DISTINCT products.id_product AS id, products.name AS title, products.price, products.old_price, products.main_image AS main_image
            FROM products
            WHERE products." . $temp . " = 1 AND products.is_active = 1
            ORDER BY products.name";
    $query = dbQuery($sql, ['group' => $group]);
    $products = $query -> fetchAll();
    return $products === false ? null : $products;
}

// Выдает продукты относящиеся к определенной подкатегории на странице каталога с учетом is_active
function getProductsBySubCategoryAndSort(string $id_category, array  $nameSubCategories, string $sort = '') : ?array {
    $queryArray = ['id_c' => $id_category];             // задаем первый подстановочный параметр подготовительного запроса
    if ($nameSubCategories[0] === 'all') {
        $querySubCat = 'categories.parent_id = :id_c)';
    } else {
        $querySubCat = 'categories.name_translit = :name_sc0';
        for ($i = 0; $i <= count($nameSubCategories) - 1; $i++) {
            $queryArray['name_sc' . $i] = $nameSubCategories[$i];
            if ($i > 0) $querySubCat = $querySubCat . ' OR categories.name_translit = :name_sc' . $i;
        }
        $querySubCat = $querySubCat . ') AND categories.parent_id = :id_c';
    }
    $querySort = $sort === '' ? 'main_image DESC' : $sort . ' ASC';
    if ($sort === 'price_to_max') $querySort = 'price ASC';
    if ($sort === 'price_to_min') $querySort = 'price DESC';
    $sql = "SELECT DISTINCT id, title, price, old_price, main_image, article, popular
            FROM (
                SELECT products.id_product AS id, products.name AS title, products.price, products.old_price, is_active,
                       products.article as article, products.index_popular as popular, products.main_image AS main_image
                FROM `categories`
                INNER JOIN productsandcategories ON productsandcategories.id_category = categories.id_category
                INNER JOIN products ON products.id_product = productsandcategories.id_product
                WHERE (" . $querySubCat . "
            ) AS a
            WHERE a.is_active = 1
            ORDER BY " . $querySort;
    $query = dbQuery($sql, $queryArray);
    $products = $query -> fetchAll();
    return $products === false ? null : $products;
}

// Выдает полную информацию об одном продукте
function getProductOne(string $id_product) : ?array {
// запрос на основную выборку по товару
    $sql = "SELECT id, article, title, price, old_price, weight, volume, height, width, depth, life, stock, package, 
        description, description_short, scope, recommendation, structure, a.parent_id as category_id,
	(SELECT categories.name
	FROM categories
	WHERE categories.id_category = a.parent_id) as category,
        subcategory_id, subcategory, subcategory_translit, main_image, is_popular, is_new, is_sale, popular, is_active 
	FROM (
		SELECT products.id_product AS id, 
		       products.article as article,
		       products.name AS title,
		       products.price,
		       products.old_price,
		       products.weight as weight,
		       products.volume as volume,
		       products.height as height,
		       products.width as width,
		       products.depth as depth,
		       products.shelf_life as life,
		       products.quantity_stock as stock,
		       products.in_package as package,
		       products.description as description,
		       products.description_short as description_short,
		       products.scope as scope,
		       products.recommendation as recommendation,
		       products.structure as structure,
		       categories.parent_id as parent_id,
		       categories.id_category as subcategory_id,
		       categories.name as subcategory,
		       categories.name_translit as subcategory_translit,
		       products.is_popular as is_popular,
		       products.is_new as is_new,
		       products.is_sale as is_sale,
		       products.index_popular as popular,
		       products.is_active as is_active,
		       products.main_image AS main_image
		FROM `categories`
		INNER JOIN productsandcategories ON productsandcategories.id_category = categories.id_category
		INNER JOIN products ON products.id_product = productsandcategories.id_product
		WHERE products.id_product = :id
	) AS a";
    $query = dbQuery($sql, ['id' => $id_product]);
    $products = $query -> fetchAll();

// запрос на выборку имеющихся URL фото по товару, для предотвращения пересекающегося множества (видео, фото, серт)
    $sql_images = "SELECT product_photos.url as url
                   FROM product_photos
                   WHERE product_photos.id_product = :id
                   ORDER BY product_photos.url";
    $query = dbQuery($sql_images, ['id' => $id_product]);
    $response_images = $query -> fetchAll();
    $temp_arr = [];
    foreach ($response_images as $image) {
        array_push($temp_arr, $image['url']);
    }
    $response_images = $temp_arr;

// запрос на выборку имеющихся URL видео по товару
    $sql_videos = "SELECT product_videos.url as url
                   FROM product_videos
                   WHERE product_videos.id_product = :id
                   ORDER BY product_videos.url";
    $query = dbQuery($sql_videos, ['id' => $id_product]);
    $response_videos = $query -> fetchAll();
    $temp_arr = [];
    foreach ($response_videos as $video) {
        array_push($temp_arr, $video['url']);
    }
    $response_videos = $temp_arr;

// запрос на выборку имеющихся URL сертификатов по товару
    $sql_certificates = "SELECT product_certificates.url as url
                         FROM product_certificates
                         WHERE product_certificates.id_product = :id
                         ORDER BY product_certificates.url";
    $query = dbQuery($sql_certificates, ['id' => $id_product]);
    $response_certificates = $query -> fetchAll();
    $temp_arr = [];
    foreach ($response_certificates as $certificate) {
        array_push($temp_arr, $certificate['url']);
    }
    $response_certificates = $temp_arr;

// запрос на выборку похожих продуктов, только id_product_similar
    $sql_similar = "SELECT product_similar.id_product_similar as similar
                         FROM product_similar
                         WHERE product_similar.id_product = :id
                         ORDER BY similar";
    $query = dbQuery($sql_similar, ['id' => $id_product]);
    $response_similar = $query -> fetchAll();
    $temp_arr = [];
    foreach ($response_similar as $similar) {
        array_push($temp_arr, $similar['similar']);
    }
    $response_similar = $temp_arr;

    $products[0]['images'] = $response_images;
    $products[0]['videos'] = $response_videos;
    $products[0]['certificates'] = $response_certificates;
    $products[0]['similar'] = $response_similar;

    return $products[0] === false ? null : $products[0];
}

// Выдает похожие продукты целевому продукту, полная инфа с учетом is_active
function getActiveSimilarProducts(string $id_product) : ?array {
    // запрос на выборку похожих продуктов
    $sql_similar = "SELECT products.id_product, products.article, products.main_image, products.name
                    FROM (
	                    SELECT DISTINCT product_similar.id_product_similar AS similar, product_similar.id_product
	                    FROM products, product_similar
	                    WHERE products.id_product = product_similar.id_product AND products.id_product = :id
                    ) AS a 
                    INNER JOIN products ON products.id_product = a.similar
                    WHERE products.is_active = 1";
    $query = dbQuery($sql_similar, ['id' => $id_product]);
    $similar = $query -> fetchAll();
    return $similar === false ? null : $similar;
}

// Выдает массу продукта по его id
function getWeightProduct(string $id_product) : ?string {
    $sql = "SELECT weight
            FROM products
            WHERE id_product = :id_product";
    $query = dbQuery($sql, ['id_product' => $id_product]);
    $weight = $query -> fetchAll();
    return $weight === false ? null : $weight[0]['weight'];
}