<?php

// Выдает подкатегории определенной категории
function getSubCategories(string $id) : ?array {
    $sql = "SELECT categories.name, categories.name_translit
            FROM categories
            WHERE categories.parent_id = :id
            ORDER BY name";
    $query = dbQuery($sql, ['id' => $id]);
    $subCategories = $query -> fetchAll();
    return $subCategories === false ? null : $subCategories;
}

// Выдает всю информацию о каждой главной категории (для view)
function getMainCategoriesInfo() : ?array {
    $sql = "SELECT id_category as id, categories.name as name, categories.name_translit as translit, description,
                   description_short as desc_short, url_main_img as main_img, url_mini_img as mini_img
            FROM categories
            WHERE categories.parent_id = 0
            ORDER BY id_category";
    $query = dbQuery($sql);
    $info = $query -> fetchAll();
    return $info === false ? null : $info;
}

// Выдает есть ли данная подкатегория по id категории и транслиту подкатегории
function isSubCategory(string $id, string $translit) : bool {
    $sql = "SELECT categories.name
            FROM categories
            WHERE categories.parent_id = :id AND categories.name_translit = :translit";
    $query = dbQuery($sql, ['id' => $id, 'translit' => $translit]);
    $subCategory = $query -> fetchAll();
    return empty($subCategory[0]) ? false : true;
}