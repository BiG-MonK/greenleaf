<ul class="social">
    <li class="social__item">
        <a class="social__link social__link--vk"
           href="https://vk.me/id414810996"
           target="_blank"
           aria-label="переход на в контакте">
            <svg>
                <use xlink:href="/img/sprite.svg#soc_vk"></use>
            </svg>
        </a>
    </li>
    <li class="social__item">
        <a class="social__link social__link--ok"
           href="https://ok.ru/profile/527366404533"
           target="_blank"
           aria-label="переход на одноклассники">
            <svg>
                <use xlink:href="/img/sprite.svg#soc_ok"></use>
            </svg>
        </a>
    </li>
    <li class="social__item">
        <a class="social__link social__link--inst"
           href="https://www.instagram.com/irina.melich"
           target="_blank"
           aria-label="переход на инстаграмм">
            <svg>
                <use xlink:href="/img/sprite.svg#soc_instagram"></use>
            </svg>
        </a>
    </li>
    <li class="social__item">
        <a class="social__link social__link--tg"
           href="https://t.me/meeelich"
           target="_blank"
           aria-label="переход на телеграмм">
            <svg>
                <use xlink:href="/img/sprite.svg#soc_telegram"></use>
            </svg>
        </a>
    </li>
    <li class="social__item">
        <a class="social__link social__link--wts"
           href="https://wa.me/79236442429"
           target="_blank"
           aria-label="переход на вотсап">
            <svg>
                <use xlink:href="/img/sprite.svg#soc_whatsapp"></use>
            </svg>
        </a>
    </li>
</ul> <!-- /.social -->