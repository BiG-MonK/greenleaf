<div class="marketing">
    <div class="marketing__image">
        <img src="/img/main/product-image.jpg" alt="product 1">
    </div>
    <div class="marketing__text">
        <span class="marketing__description">Someone purchaed a</span>
        <h3 class="marketing__title">Faux shearling double-breasted coat</h3>
        <span class="marketing__when-from">15 minutes ago London, Great Britain</span>
    </div>
    <div class="marketing__options">
        <button class="marketing__close btn-reset">
            <svg class="marketing__close-icon">
                <use xlink:href="/img/sprite.svg#close" aria-label="close marketing window"></use>
            </svg>
        </button>
        <button class="marketing__eye btn-reset">
            <svg class="marketing__eye-icon">
                <use xlink:href="/img/sprite.svg#eye" aria-label="hide marketing window"></use>
            </svg>
        </button>
    </div>
</div>