<div class="account-data <?=is_null($accountInfo) ? "waves" : "" ?>">
    <div class="account-data__item">
        <span class="account-data__label">Имя:</span>
        <span class="account-data__info" data-info-set="first_name"><?=$user['first_name'] ?></span>
    </div>
    <div class="account-data__item">
        <span class="account-data__label">Фамилия:</span>
        <span class="account-data__info" data-info-set="last_name"><?=$user['last_name'] ?></span>
    </div>
    <div class="account-data__item">
        <span class="account-data__label">Отчество:</span>
        <span class="account-data__info" data-info-set="middle_name"><?=$user['middle_name'] ?></span>
    </div>
    <div class="account-data__item">
        <span class="account-data__label">Email адрес:</span>
        <span class="account-data__info" data-info-set="email"><?=$user['email'] ?></span>
    </div>
    <div class="account-data__item">
        <span class="account-data__label">Телефон:</span>
        <span class="account-data__info" data-info-set="phone"><?=$user['phone'] ?></span>
    </div>
</div> <!-- /.account-data -->