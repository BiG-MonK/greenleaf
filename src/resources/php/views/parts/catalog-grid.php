<div class="catalog-grid" id="catalog">
    <div class="catalog-grid__props catalog-props">
        <div class="catalog-props__top">
            <button class="btn-reset catalog-mobile-filters">
                <svg height="394pt" viewBox="-5 0 394 394.00003" width="394pt" xmlns="http://www.w3.org/2000/svg">
                    <path d="m367.820312 0h-351.261718c-6.199219-.0117188-11.878906 3.449219-14.710938 8.960938-2.871094
                    5.585937-2.367187 12.3125 1.300782 17.414062l128.6875 181.285156c.042968.0625.089843.121094.132812.183594
                    4.675781 6.3125 7.207031 13.960938 7.21875 21.816406v147.800782c-.027344 4.375 1.691406 8.582031 4.773438
                    11.6875 3.085937 3.101562 7.28125 4.851562 11.65625 4.851562 2.222656-.003906 4.425781-.445312
                    6.480468-1.300781l72.3125-27.570313c6.476563-1.980468 10.777344-8.09375
                    10.777344-15.453125v-120.015625c.011719-7.855468 2.542969-15.503906
                    7.214844-21.816406.042968-.0625.089844-.121094.132812-.183594l128.691406-181.289062c3.667969-5.097656
                    4.171876-11.820313 1.300782-17.40625-2.828125-5.515625-8.511719-8.9765628-14.707032-8.964844zm0 0" />
                </svg>
                <span>Фильтр</span>
            </button>
            <div class="catalog-props__columns catalog-columns">
                <span class="catalog-columns__text">Количество столбцов:</span>
                <ul class="catalog-columns__list">
                    <li class="catalog-columns__item">
                        <button class="btn-reset main-link catalog-columns__btn" data-columns="3">3</button>
                    </li>
                    <li class="catalog-columns__item">
                        <button class="btn-reset main-link catalog-columns__btn" data-columns="4">4</button>
                    </li>
                    <li class="catalog-columns__item">
                        <button class="btn-reset main-link catalog-columns__btn catalog-columns__btn--current" data-columns="5">5</button>
                    </li>
                    <li class="catalog-columns__item">
                        <button class="btn-reset main-link catalog-columns__btn" data-columns="6">6</button>
                    </li>
                </ul>
            </div> <!-- /.catalog-props__columns -->

            <div class="catalog-props__options">
                <div class="catalog-props__quantity-label">Показать по</div>
                <div class="custom-select catalog-props__quantity js-catalog-props__quantity" tabindex="0">
                    <div class="custom-select__top">15</div>
                    <div class="custom-select__dropdown">
                        <ul class="custom-select__list">
                            <li class="custom-select__item">15</li>
                            <li class="custom-select__item">20</li>
                            <li class="custom-select__item">25</li>
                            <li class="custom-select__item">30</li>
                        </ul>
                    </div>
                </div> <!-- /.custom-select .catalog-props__quantity -->
                <div class="catalog-props__sort-label">Сортировать</div>
                <div class="custom-select catalog-props__sort" tabindex="0">
                    <div class="custom-select__top" data-sort="popular">По популярности</div>
                    <div class="custom-select__dropdown">
                        <ul class="custom-select__list">
                            <li class="custom-select__item" data-sort="popular">По популярности</li>
                            <li class="custom-select__item" data-sort="title">По наименованию</li>
                            <li class="custom-select__item" data-sort="article">По артиклю</li>
                            <li class="custom-select__item" data-sort="price_to_max">По возрастанию цены</li>
                            <li class="custom-select__item" data-sort="price_to_min">По убыванию цены</li>
                        </ul>
                    </div>
                </div> <!-- /.custom-select .catalog-props__sort -->
            </div>

        </div> <!-- /.catalog-props__top -->
        <div class="catalog-props__choice catalog-choice">
            <button class="btn-reset catalog-choice__clear">Очистить фильтр</button>
        </div>
    </div> <!-- /.catalog-grid__props -->
    <ul class="catalog-grid__list"
        data-grid-columns="5"
        data-category-id="<?=$currentIdCategory?>"
        data-subcategory-name="<?=$currentNameSubCategory?>"></ul>
    <div class="catalog-grid__empty js-catalog-grid-empty">По данному набору фильтров<br /> товар не найден...</div>
    <ul class="custom-pagination">
        <li class="custom-pagination__item js-custom-pagination__item--prev">
            <a href="#catalog" class="custom-pagination__link btn-default">
                <svg class="">
                    <use xlink:href="/img/sprite.svg#angle-left"></use>
                </svg>
                <svg class="">
                    <use xlink:href="/img/sprite.svg#angle-left"></use>
                </svg>
            </a>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--begin">
            <a href="#catalog-categories" class="custom-pagination__link btn-default js-custom-pagination__link--begin custom-pagination__link--current">1</a>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--more-prev">
            <div class="custom-pagination__link">..</div>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--one">
            <a href="#catalog-categories" class="custom-pagination__link btn-default js-custom-pagination__link--one">2</a>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--two">
            <a href="#catalog-categories" class="custom-pagination__link btn-default js-custom-pagination__link--two">3</a>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--tree">
            <a href="#catalog-categories" class="custom-pagination__link btn-default js-custom-pagination__link--tree">4</a>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--more-next">
            <div class="custom-pagination__link btn-default">..</div>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--end">
            <a href="#catalog-categories" class="custom-pagination__link btn-default js-custom-pagination__link--end">max</a>
        </li>
        <li class="custom-pagination__item js-custom-pagination__item--next">
            <a href="#catalog-categories" class="custom-pagination__link btn-default">
                <svg class="">
                    <use xlink:href="/img/sprite.svg#angle-right"></use>
                </svg>
                <svg class="">
                    <use xlink:href="/img/sprite.svg#angle-right"></use>
                </svg>
            </a>
        </li>
    </ul> <!-- /.custom-pagination -->
</div> <!-- /.catalog-grid -->