<section class="hero" data-speed=".35">
    <div class="container hero__container">
        <h1 class="visually-hidden">GreenLeaf - магазин в Кулунде!</h1>
        <div class="hero__video">
            <video class="hero__video-media" autoplay loop muted poster playsinline controlslist="nodownload">
                <source src="/video/hero.webm" type="video/webm">
                <source src="/video/hero.mp4" type="video/mp4">
                Ваш браузер не поддерживает воспроизведение видео.
            </video>
            <div class="hero__content" data-speed=".13">
                <a href="/catalog/category/1" class="hero__to-shop button"><span class="hero__green">Green</span>Leaf</a>
            </div>
        </div>
    </div>
</section>