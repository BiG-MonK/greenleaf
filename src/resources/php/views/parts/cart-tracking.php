<section class="tab-content__tab tracking <? echo $tab === 'tracking' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#tracking">
    <h2 class="visually-hidden">Order Tracking</h2>
    <form action="#" class="tracking__form">
        <p class="tracking__description">To track your order please enter your Order ID in the box below
            and press the "Track" button. This was given to you on your receipt and in the
            confirmation email you should have received.</p>
        <div class="tracking__fields">
            <label for="order-id">Order ID</label>
            <input type="text"
                   class="tracking__order-id form-field"
                   id="order-id"
                   placeholder="Found in your order confirmation email"
                   name="order-id">
            <label for="tracking-email">Billing Email</label>
            <input type="email"
                   class="tracking__email form-field"
                   id="tracking-email"
                   placeholder="Email you used during checkout."
                   name="tracking-email">
            <button class="tracking__submit btn-reset" aria-label="Track">Track</button>
        </div> <!-- /.tracking__fields -->
    </form> <!-- /.tracking__form -->
</section> <!-- /.tracking -->
