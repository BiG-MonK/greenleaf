<section class="banner-greenleaf">
    <h2 class="visually-hidden">О компании Greenleaf</h2>
    <div class="container banner-greenleaf__container">
<!--        <img class="banner-greenleaf__img" src="/img/main/banner_greenleaf.jpg" alt="природа">-->
        <a href="/company" class="banner-greenleaf__ref">
            <h3 class="banner-greenleaf__title">Почему наш выбор -
                <span class="banner-greenleaf__green">Green</span>Leaf</h3>
        </a>
    </div>
</section>
