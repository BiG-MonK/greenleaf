<div class="catalog-filters">
    <div class="catalog-filter" data-filter="subcategory">
        <div class="catalog-filter__top">
            <div class="catalog-filter__caption">
                <h3 class="catalog-filter__title">Подразделы</h3>
                <span class="catalog-filter__quantity quantity">0</span>
            </div>
        </div>
        <div class="catalog-filter__bottom">
            <ul class="catalog-filter__items"></ul>
        </div> <!-- /.catalog-filter__bottom -->
    </div> <!-- /.catalog-filter -->
</div><!-- /.catalog-filters -->
