<div class="card-content__bottom">
    <div class="container container-narrow">
        <div class="card-description">
            <? if ($product['description'] || $product['scope'] || $product['recommendation'] || $product['structure'] || $product['certificates']) { ?>
            <div class="card-description__wrapper">
                <div class="card-description__wrapper-title">О продукте</div>
                <? if ($product['description']) { ?>
                <h3 class="card-description__heading">Описание</h3>
                <p class="card-description__content"><?=$product['description']?></p>
                <? } ?>
                <? if ($product['scope']) { ?>
                <h3 class="card-description__heading">Сфера применения</h3>
                <p class="card-description__content"><?=$product['scope']?></p>
                <? } ?>
                <? if ($product['recommendation']) { ?>
                <h3 class="card-description__heading">Рекомендации по применению</h3>
                <p class="card-description__content"><?=$product['recommendation']?></p>
                <? } ?>
                <? if ($product['structure']) { ?>
                <h3 class="card-description__heading">Состав</h3>
                <p class="card-description__content"><?=$product['structure']?></p>
                <? } ?>
                <? if ($product['certificates']) { ?>
                <h3 class="card-description__heading">Сертификаты</h3>
                <div class="card-description__content">
                    <ul class="card-description__certifications">
                        <? foreach ($product['certificates'] as $certificate) { ?>
                            <li class="certifications__item">
                                <img src="<?= $certificate?>" alt="сертификат">
                            </li>
                        <? } ?>
                    </ul>
                </div>
                <? } ?>
            </div>
            <? } ?>
        </div> <!-- /.card-description -->
        <? if ($product['similar']) { ?>
        <div class="card-related">
            <h2 class="card-related__title">Рекомендуем так же:</h2>
            <div class="card-related__slider">
                <ul class="swiper-wrapper card-related__list" data-target-similar-id="<?= $product['id'] ?>"></ul>
                <div class="swiper-pagination related-pag"></div>
            </div> <!-- /.card-related__slider -->
        </div> <!-- /.card-related -->
        <? } ?>
    </div> <!-- /.container -->
</div> <!-- /.card-content__bottom -->
