<div class="catalog-categories" id="catalog-categories">
    <ul class="catalog-categories__list dividing-line">

        <? foreach($categoriesInfo as $category) { ?>
        <li class="catalog-categories__item catalog-categories__item--<?=$category['color']?>" data-category="<?=$category['id']?>">
            <div class="catalog-categories__image">
                <img src="<?=$category['mini_img']?>" alt="<?=$category['name']?>">
            </div>
            <h4 class="catalog-categories__title"><?=$category['name']?></h4>
        </li>
        <? } ?>

    </ul> <!-- /.catalog-filter__categories -->
</div> <!-- /.catalog-filter__categories -->