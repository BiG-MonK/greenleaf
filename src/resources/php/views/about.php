<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/free-delivery.php') ?>
    <? include('php/views/parts/common/header.php') ?>

    <main class="main about-page">
        <section class="about" id="about">
            <div class="container about__container">
                <h2 class="about__title main-title page-title">О нас</h2>

                <div class="about__block about__block-1">
                    <div class="about__text-wrap">
                        <div class="about__text">
                            <p>Мы проживаем в посёлке Кулунда Алтайского края.</p>
                            <p>На этом сайте представляем для Вас эко-продукцию класса премиум от компании
                                <?include('php/views/parts/common/logo-in-text.php')?>.
                                Являясь партнёрами компании, мы по достоинству оценили качество продукции,
                                которая безопасна для нашего здоровья и окружающей среды. Предлагаем и Вам познакомиться
                                с большим ассортиментом товаров повседневного спроса.</p>
                        </div>
                    </div> <!-- /.about__text-wrap -->
                </div> <!-- /.about__block -->

                <div class="about__block about__block-2">
                    <div class="about__image">
                        <img src="/img/about/office_inside_1.jpg" alt="Фото офиса с улицы">
                    </div>
                    <div class="about__text-wrap">
                        <div class="about__text">
                            <p>Почему же мы выбрали сотрудничество с
                                <?include('php/views/parts/common/logo-in-text.php')?>?</p>
                            <p><b>Во-первых</b>, это компания - производитель, поэтому цены на товары высокого качества у неё самые доступные.</p>
                            <p><b>Во-вторых</b>, защита от подделок: компания гарантирует нам, что полученный товар соответствует
                                всем требованиям, подтверждая это наличием сертификатов.</p>
                            <p><b>И в третьих</b>, это научная база и современные технологии Китая, Японии, Швейцарии и других стран.
                                Новейшие разработки внедряются компанией в прозводство эксклюзивных эко-товаров.</p>
                            <p>Если Вас интересует сотрудничество с
                                <?include('php/views/parts/common/logo-in-text.php')?>,
                                свяжитесь с нами, и мы расскажем об этой возможности.</p>
                        </div>
                    </div> <!-- /.about__text-wrap -->
                </div> <!-- /.about__block -->

                <div class="about__block about__block-3">

                    <div class="about__text-wrap">
                        <div class="about__text">
                            <p>Так как <?include('php/views/parts/common/logo-in-text.php')?> является лидером биохимической продукции Китая и уже с 1998 года на рынке,
                                то ассортимент товаров компании очень большой, уже более 4000 наименований.</p>
                            <p>Мы предлагаем вам эко-товары основных направлений: средства для стирки и уборки,
                                для личной гигиены и здоровья, уходовая косметика. Есть продукция для любых возрастных категорий,
                                для детей и взрослых. Со временем мы планируем расширять ассортимент товаров, будет много
                                новинок и интересных предложений.</p>
                            <p>У нас Вы сможете также получить подробную консультацию о том, как пользоваться продукцией,
                                чтобы получить желаемый результат. Свяжитесь с нами, если у Вас есть вопросы.</p>
                        </div>
                    </div> <!-- /.about__text-wrap -->
                </div> <!-- /.about__block -->
                <div class="about__map">
                    <h3 class="company__block-title about__map-title">Мы находимся тут:</h3>
                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A65053b0056d4660e0b41c24671adcad7c9ec56f1bc82ccbf0d7b5e670db319e4&amp;source=constructor" frameborder="0"></iframe>
                </div>

            </div> <!-- /.about__container -->
        </section> <!-- /.about -->
    </main>

    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>
