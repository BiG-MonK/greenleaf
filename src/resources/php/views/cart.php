<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/free-delivery.php') ?>
    <? include('php/views/parts/common/header.php') ?>
    <main class="main cart-page">
        <div class="tab-navigation cart-navigation">
            <ul class="tab-navigation__list cart-navigation__list">
                <li class="tab-navigation__item">
                    <a href="#shopping"
                       data-url="/cart/shopping"
                       class="main-link tab-navigation__link <? echo $tab === 'shopping' ? 'tab-navigation__link--active' : ''?>">
                        Корзина(<span class="cart-navigation__link-quantity">0</span>)</a>
                </li>
                <li class="tab-navigation__item">
                    <a href="#checkout"
                       data-url="/cart/checkout"
                       class="main-link tab-navigation__link <? echo $tab === 'checkout' ? 'tab-navigation__link--active' : ''?>">
                        Заказ</a>
                </li>
<!--                <li class="tab-navigation__item">-->
<!--                    <a href="#tracking"-->
<!--                       data-url="/cart/tracking"-->
<!--                       class="main-link tab-navigation__link --><?// echo $tab === 'tracking' ? 'tab-navigation__link--active' : ''?><!--">-->
<!--                        Отслеживание заказа</a>-->
<!--                </li>-->
            </ul>
        </div> <!-- /.cart-navigation -->
        <div class="tab-content cart-content">
            <? include('php/views/parts/cart-shopping.php') ?>
            <? include('php/views/parts/cart-checkout.php') ?>
            <? include('php/views/parts/cart-tracking.php') ?>
        </div> <!-- /.cart-content -->
    </main>
    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>