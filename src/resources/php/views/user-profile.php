<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/free-delivery.php') ?>
    <? include('php/views/parts/common/header.php') ?>
    <main class="main profile-page">
        <h1 class="profile-page__title page-title">Личный кабинет</h1>
        <div class="tab-navigation profile-navigation">
            <ul class="tab-navigation__list">
                <? if($user['is_admin']) { ?>
                    <li class="tab-navigation__item">
                        <a href="#admin-panel"
                           data-url="/profile/admin-panel"
                           class="main-link tab-navigation__link <? echo $tab === 'admin-panel' ? 'tab-navigation__link--active' : ''?>">Админ-панель</a>
                    </li>
                <? } ?>
                <li class="tab-navigation__item">
                    <a href="#orders"
                       data-url="/profile/orders"
                       class="main-link tab-navigation__link <? echo $tab === 'orders' ? 'tab-navigation__link--active' : ''?>">Заказы</a>
                </li>
                <li class="tab-navigation__item">
                    <a href="#address"
                       data-url="/profile/address"
                       class="main-link tab-navigation__link <? echo $tab === 'address' ? 'tab-navigation__link--active' : ''?>">Адрес</a>
                </li>
                <li class="tab-navigation__item">
                    <a href="#account-details"
                       data-url="/profile/account"
                       class="main-link tab-navigation__link <? echo $tab === 'account' ? 'tab-navigation__link--active' : ''?>">Детали аккаунта</a>
                </li>
                <li class="tab-navigation__item">
                    <a href="/logout" class="main-link">Выход</a>
                </li>
            </ul>
        </div> <!-- /.profile-navigation -->

        <div class="tab-content profile-content">

            <?
                // сюда приходит переменная $categoriesArrDim содержащая
                // [0] => [id_category] => ''
                //        [name] => ''
                //        [parent_id] => ''
                //        [subCategory] => Array => [0] => [id_category] => ''
                //                                              ...
                if($user['is_admin']) include('php/views/parts/admin-panel.php'); ?>

            <section class="tab-content__tab orders <? echo $tab === 'orders' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#orders">
                <h2 class="visually-hidden">Мои заказы</h2>
                <? if (empty($ordersInfo)) { ?>
                    <div class="profile-content__messages prof-messages">
                        <div class="prof-messages__item">
                            <div class="prof-messages__text">Заказов пока еще нет.</div>
                        </div>
                    </div> <!-- /.prof-messages -->
                <? } else { ?>
                <div class="order-message__wrap-details">
                    <? foreach ($ordersInfo as $order) { ?>
                        <article class="order"
                                 data-status-id="<?= $order['id_status']?>"
                                 data-id-order="<?= $order['id_order']?>">
                            <div class="order__status">Статус заказа: <?= $order['status_name']?></div>
                            <div class="order__line-details">
                                <div class="order__line-item">
                                    <span class="order__item-title">Номер заказа:</span> <?= $order['id_order']?>
                                </div>
                                <div class="order__line-item">
                                    <span class="order__item-title">Дата:</span> <?= preg_split('/\s+/', $order['date_placed'])[0] ?>
                                </div>
                                <div class="order__line-item">
                                    <span class="order__item-title">Итоговая сумма:</span>
                                    ₽ <?= number_format($order['sum_total_rub'], 2, '.', ' ') ?>
                                </div>
                                <div class="order__line-item">
                                    <span class="order__item-title">Метод доставки:</span>
                                    <?= ($order['method_delivery'] === 'pickup') ? "Самовывоз" : "Почтовая служба" ?>
                                </div>
                            </div>
                            <? if ($order['method_delivery'] === 'delivery') {
                                $userInfoAddress = isset($order['user_info'])
                                    ? json_decode($order['user_info'], true)
                                    : [ 'country' => $user['country'],
                                        'city' => $user['city'],
                                        'street' => $user['street'],
                                        'postcode' => $user['postcode'] ] ?>
                            <div class="order__line-details">
                                <div class="order__line-item">
                                    <span class="order__item-title">Страна: </span> <?= $userInfoAddress['country'] ?>
                                </div>
                                <div class="order__line-item">
                                    <span class="order__item-title">Населенный пункт: </span> <?= $userInfoAddress['city']?>
                                </div>
                                <div class="order__line-item">
                                    <span class="order__item-title">Улица, дом, кв.: </span> <?= $userInfoAddress['street']?>
                                </div>
                                <div class="order__line-item">
                                    <span class="order__item-title">ИНДЕКС: </span> <?= $userInfoAddress['postcode']?>
                                </div>
                            </div>
                            <? } ?>
                            <div class="order__order-details order-details is-reduce-mh" data-id-order="<?= $order['id_order']?>">
                                <h3 class="order-details__title">Детали заказа</h3>
                                <div class="order-details__wrap-details">
                                    <div class="order-details__sub-title">Заказанные товары</div>
                                    <ul class="order-details__product-list">
                                        <? foreach (getProductsByOrder($order['id_order']) as $product) { ?>
                                            <li class="order-details__product-item">
                                                <a class="order-details__product-name"
                                                   href="/product/<?= $product['id_product']?>"
                                                   target="_blank"><?= $product['name']?></a>
                                                <div class="order-details__product-quantity">x <?= $product['quantity']?></div>
                                                <div class="order-details__product-price">
                                                    <span class="js-conventional-unit">₽</span>
                                                    <span class="order-details__price-sum">
                                                    <?= number_format($product['sum_rub'], 2, '.', ' ') ?>
                                                </span>
                                                </div>
                                            </li>
                                        <? } ?>
                                    </ul> <!-- /.check__product-list -->
                                    <div class="order-details__line order-details__total-sum">
                                        <div class="order-details__sum-wrap">
                                            <span class="js-conventional-unit">₽</span>
                                            <span class="order-details__sum">
                                            <?= number_format($order['sum_total_rub'] - $order['sum_delivery'], 2, '.', ' ') ?>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="order-details__line">
                                        <div class="order-details__sub-title">Доставка</div>
                                        <div class="order-details__sum-wrap">
                                            <span class="js-conventional-unit">₽</span>
                                            <span class="order-details__sum">
                                            <?= number_format($order['sum_delivery'], 2, '.', ' ') ?>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="order-details__line">
                                        <div class="order-details__sub-title">Итого</div>
                                        <div class="order-details__sum-wrap">
                                            <span class="js-conventional-unit">₽</span>
                                            <span class="order-details__sum">
                                            <?= number_format($order['sum_total_rub'], 2, '.', ' ') ?>
                                        </span>
                                        </div>
                                    </div>
                                </div> <!-- /.order-details__wrap-details -->
                            </div>
                        </article>

                    <? } ?>
                    </div>
                <? } ?>
                <div class="prof-messages__return">
                    <a href="/catalog/category/1" class="prof-messages__btn btn-reset btn-default">Вернуться в магазин</a>
                </div>
            </section> <!-- /.orders -->

            <section class="tab-content__tab address <? echo $tab === 'address' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#address">
                <h2 class="visually-hidden">Адрес</h2>
                <div class="profile-content__messages prof-messages">
                    <div class="prof-messages__item">
                        <div class="prof-messages__text">
                            <?=($addressInfo) ? "Следующий адрес будет использоваться на странице оформления заказа по умолчанию"
                                          : "Чтобы мы могли Вам отправить товар, добавьте адрес доставки" ?></div>
                    </div>
                </div> <!-- /.prof-messages -->
                <? include('php/views/parts/common/profile-data/address-data.php') ?>
                <label class="address__data-edit js-address__data-edit custom-checkbox <?=($addressInfo) ? "" : "check-box-form--active" ?>">
                    <input type="checkbox" class="custom-checkbox__input visually-hidden" <?=($addressInfo) ? "" : "checked" ?>>
                    <span class="custom-checkbox__text"><?=($addressInfo) ? "Изменить адрес доставки" : "Добавить адрес доставки" ?></span>
                </label>
                <form class="address-form <?=($addressInfo) ? "is-reduce" : "" ?>">
                    <h3 class="address-form__title form__title">Детали доставки</h3>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="country">Страна *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="country"
                               placeholder="Например: Россия"
                               name="country">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="city">Населенный пункт *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="city"
                               placeholder="Например: г.Барнаул"
                               name="city">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="street">Улица, дом, кв. *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="street"
                               placeholder="Например: ул.Ленина 33, кв. 42"
                               name="street">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="postcode">ИНДЕКС *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="postcode"
                               placeholder="Например: 666666"
                               name="postcode">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <button type="submit"
                            class="address-form__edit-btn btn-reset btn-default <?=($addressInfo) ? "" : "waves" ?>">
                        <?= ($addressInfo) ? "Сохранить" : "Добавить" ?>
                    </button>
                    <div class="response-message is-reduce">Заполните поля формы</div> <!-- /.auth-message -->
                </form> <!-- /.address-form -->
            </section> <!-- /.address -->

            <section class="tab-content__tab account <? echo $tab === 'account' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#account-details">
                <h2 class="visually-hidden">Детали аккаунта</h2>
                <div class="profile-content__messages prof-messages">
                    <div class="prof-messages__item">
                        <div class="prof-messages__text">
                            <?=($accountInfo) ? "Следующий контакт будет использоваться на странице оформления заказа по умолчанию"
                                : "Чтобы мы знали как к Вам обращаться и для упрощения оформления заказа, добавьте свои данные контакта" ?>
                        </div>
                    </div>
                </div> <!-- /.prof-messages -->
                <div class="account__data-wrapper">
                    <? include('php/views/parts/common/profile-data/account-data.php') ?>
                </div><!-- /.account__data-wrapper -->

                <div class="account__edit-block">
                    <div class="account__data-edit-wrapper">
                        <label class="account__data-edit js-account__data-edit custom-checkbox <?=($accountInfo) ? "" : "check-box-form--active" ?>">
                            <input type="checkbox" class="custom-checkbox__input visually-hidden" <?=($accountInfo) ? "" : "checked" ?>>
                            <span class="custom-checkbox__text"><?=($accountInfo) ? "Изменить данные по умолчанию" : "Добавить данные по умолчанию" ?></span>
                        </label>
                        <form class="account-form form__edit-data-acc <?=($accountInfo) ? "is-reduce" : "" ?>">
                        <h3 class="account-form__title form__title">Данные учетной записи</h3>
                        <div class="form__item">
                            <label class="account-form__label form__label" for="first_name">Имя *</label>
                            <input type="text"
                                   class="account-form__input form-field js-required"
                                   id="first_name"
                                   placeholder=""
                                   name="first_name">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="account-form__label form__label" for="middle_name">Отчество *</label>
                            <input type="text"
                                   class="account-form__input form-field js-required"
                                   id="middle_name"
                                   placeholder=""
                                   name="middle_name">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="account-form__label form__label" for="last_name">Фамилия *</label>
                            <input type="text"
                                   class="account-form__input form-field js-required"
                                   id="last_name"
                                   placeholder=""
                                   name="last_name">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="account-form__label form__label" for="phone">Телефон *</label>
                            <input type="text"
                                   class="account-form__input form-field js-required"
                                   id="phone"
                                   placeholder="Например: 8-999-999-99-99"
                                   name="phone">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <button type="submit"
                                class="account-form__edit-btn btn-reset btn-default <?=($accountInfo) ? "" : "waves" ?>"
                                formaction="#"
                                formmethod="POST">
                            <?= ($accountInfo) ? "Сохранить" : "Добавить" ?>
                        </button>
                        <div class="response-message is-reduce">Заполните поля формы</div> <!-- /.auth-message -->
                    </form> <!-- /.account-form -->
                    </div> <!-- /.account__data-edit-wrapper -->

                    <div class="account__pass-edit-wrapper">
                        <label class="account__pass-edit js-account__pass-edit custom-checkbox">
                            <input type="checkbox" class="custom-checkbox__input visually-hidden">
                            <span class="custom-checkbox__text">Сменить пароль</span>
                        </label>
                        <form class="account-form form__edit-password is-reduce">
                            <h3 class="account-form__title form__title">Сменить пароль</h3>
                            <div class="form__item">
                                <label class="account-form__label form__label" for="current_password">Действующий пароль</label>
                                <input type="password"
                                       class="account-form__input form-field js-required"
                                       id="current_password"
                                       name="current_password">
                                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                            </div>
                            <div class="form__item">
                                <label class="account-form__label form__label" for="new_password">Новый пароль</label>
                                <input type="password"
                                       class="account-form__input form-field js-required"
                                       id="new_password"
                                       name="new_password">
                                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                            </div>
                            <div class="form__item">
                                <label class="account-form__label form__label" for="confirm_password">Подтвердите новый пароль</label>
                                <input type="password"
                                       class="account-form__input form-field js-required"
                                       id="confirm_password"
                                       name="confirm_password">
                                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                            </div>
                            <button type="submit"
                                    class="account-form__edit-btn btn-reset btn-default"
                                    formaction="#"
                                    formmethod="POST">Сохранить
                            </button>
                            <div class="response-message is-reduce">Заполните поля формы</div> <!-- /.auth-message -->
                        </form> <!-- /.form__edit-password -->
                    </div> <!-- /.account__pass-edit-wrapper -->
                </div>

            </section> <!-- /.account -->

            <section class="tab-content__tab logout" data-section="#logout">
                <h2 class="visually-hidden">Выход</h2>
            </section> <!-- /.logout -->
        </div> <!-- /.profile-content -->
    </main>

    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>
