<?php

// Функция парсера URL адреса и подсчета передаваемых параметров
function parseUrl(string $url, array $routes) : array {
    $res = [
        'controller' => 'c_error_404',
        'params' => []
    ];
    foreach($routes as $route) {
        $matches = [];
        if(preg_match($route['test'], $url, $matches)) {
            $res['controller'] = $route['controller'];
            if(isset($route['params'])) {
                foreach($route['params'] as $name => $num) {
                    $res['params'][$name] = $matches[$num];
                }
            }
            break;
        }
    }
    return $res;
}