<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'php/core/PHPMailer-6.4.0/src/PHPMailer.php';
    require 'php/core/PHPMailer-6.4.0/src/Exception.php';

    $mail = new PHPMailer(true);
    $mail -> CharSet = 'UTF-8';
    $mail -> setLanguage('ru', 'PHPMailer-6.4.0/language');
    $mail -> IsHTML(true);

    // -- От кого письмо
    $mail -> setFrom(EMAIL_SENDER, 'GreenLeaf');