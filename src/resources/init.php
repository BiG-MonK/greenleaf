<?php

const PROTOCOL = 'https://';
const HOST = 'https://localhost:3000';
const BASE_URL = '/greenleaf/';
const POSTCODE_ADMIN = '658920';        // индекс хозяина магазина (для отправки товаров почтой)
const PHONE_ADMIN = '+7 923 644 2429';        // индекс хозяина магазина (для отправки товаров почтой)
const EMAIL_SENDER = 's-monk-s@mail.ru';        // Email с которого будеет производится отправка писем
const EMAIL_ADMIN = 'ssmonk@mail.ru';        // Email хозяина магазина (для уведомлений)
//const EMAIL_ADMIN = 'melich.i@mail.ru';        // Email хозяина магазина (для уведомлений)

const POST_MARKUP = 50;                 // наценка на почтовые отправления для выравнивания расхождений с данными сервиса
const USER_PROFILE = 'https://greenleaf/profile/account';

const DB_HOST = 'localhost';
const DB_NAME = 'greenleaf';
const DB_USER = 'mysql';
const DB_PASS = '';

const PATTERN_NUMBER = '/^[0-9]+$/';
const PATTERN_EMAIL = '/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
const PATTERN_NAME = "/^[aA-zZаёЁА-яЯ\-]+$/u";
const PATTERN_NAME_GAP = "/^[aA-zZаёЁА-яЯ\- ]+$/u";
//const PATTERN_PHONE = '/^((\+?7|8)[ \-]?)?([ \-])?(\d{6}|(\d{3}([ \-])?\d{3})?[\- ]?\d{2}[\- ]?\d{2})$/'; до подключения inputMask
const PATTERN_PHONE = '/^(\+7 \(\d{3}\) \d{3} \d{2}-\d{2})$/';
const PATTERN_POSTCODE = '/^\d{6}$/';
const PATTERN_PASSWORD = '/(?=.*[a-zA-Z])(?=.*[0-9])[0-9a-zA-Z_\W *]{8,}/';
const PATTERN_STREET = "/^[a-z0-9а-яёЁA-ZА-Я\-_\/.: \\,]{8,}$/u";
const PATTERN_COUNTRY = '/^[a-zа-яA-ZёЁА-Я\- ]{4,}$/u';
const PATTERN_CITY = '/^[a-zа-яA-ZёЁА-Я\-. ]{4,}$/u';
const PATTERN_TEXTAREA = '/(.+[\s].+)/u';


date_default_timezone_set('Asia/Barnaul');

$responseMessage = ['is_ok' => 0, 'message' => ''];

include_once('php/core/db.php');
include_once('php/core/system.php');
include_once('php/core/auth.php');
include_once('php/core/mail.php');

include_once('php/model/m_users.php');
include_once('php/model/m_orders.php');
include_once('php/model/m_sessions.php');
include_once('php/model/m_products.php');
include_once('php/model/m_categories.php');
include_once('php/model/m_auth.php');
include_once('php/model/m_validate.php');
include_once('php/model/admin-panel/m_html-elements.php');
include_once('php/model/admin-panel/m_categories.php');
include_once('php/model/admin-panel/m_products.php');
include_once('php/model/admin-panel/m_orders.php');
include_once('php/model/admin-panel/m_faq.php');
